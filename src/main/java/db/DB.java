package db;

import dao.Answer;
import dao.Question;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class DB {

  final String JDBC_DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
  final String URL = "jdbc:derby:INNOVUSDB;create=true";
  final String USERNAME = "";
  final String PASSWORD = "";

  Connection conn = null;
  Statement createStatement = null;
  DatabaseMetaData dbmd = null;

  public DB() {

    try {
      conn = DriverManager.getConnection(URL);
      if (conn != null) {
        createStatement = conn.createStatement();
      }
      dbmd = conn.getMetaData();

      ResultSet rsq = dbmd.getTables(null, "APP", "QUESTIONS", null);
      ResultSet rsa = dbmd.getTables(null, "APP", "ANSWERS", null);
      if (!rsq.next()) {
        System.out.println("OK 1");
        createStatement.execute(
            "CREATE TABLE questions(id INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), question VARCHAR(255) UNIQUE, CONSTRAINT primary_key PRIMARY KEY (id))");
        System.out.println("OK 1");
      }
      if (!rsa.next()) {
        System.out.println("OK 2");
        createStatement.execute(
            "CREATE TABLE answers(id INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), answer VARCHAR(255) , question_ID INTEGER NOT NULL)");
        System.out.println("OK 2");
      }
      rsa.close();
      rsq.close();
    } catch (SQLException ex) {
      System.out.println("Could not create DB!");
      System.out.println("" + ex);
    }
  }

  public void addAQuestion(String question) {

    try {
      String sql = "INSERT INTO questions VALUES (default, ?)";
      PreparedStatement pstmt = conn.prepareStatement(sql);
      pstmt.setString(1, question);
      pstmt.execute();
      pstmt.close();
    } catch (SQLException ex) {
      System.out.println("Could not add question! - addAQuestion");
      System.out.println("" + ex);
    }
  }

  private int getAQuestionID(String inputQuestion) {
    String sql = "";
    int questionID = 0;
    try {
      sql = "SELECT id FROM questions WHERE question=question";
      ResultSet rs = createStatement.executeQuery(sql);
      questionID = rs.getInt("id");
      rs.close();
    } catch (SQLException ex) {
      System.out.println("Could not add question! - getAQuestionID");
      System.out.println("" + ex);
    }

    return questionID;
  }

  public void addAnAnswer(String inputQuestion, String inputAnswer) {
    try {
      String sql = "INSERT INTO answers VALUES (default, ?, ?)";
      PreparedStatement pstmt = conn.prepareStatement(sql);
      pstmt.setString(1, inputAnswer);
      int inputQuestionID = getAQuestionID(inputQuestion);
      if (inputQuestionID == 0) {
        throw new SQLException("No matching question");
      }
      pstmt.setInt(2, inputQuestionID);
      pstmt.execute();
      pstmt.close();
    } catch (SQLException ex) {
      System.out.println("Could not add question! - addAnAnswer");
      System.out.println("" + ex);
    }
  }

  public HashMap<Question, List<Answer>> getQuestionAndAllAnswers(String question) {
    String sqlQuestion = "";
    String sqlAnswer = "";
    List<Answer> answers = new ArrayList<>();
    Question questionObj = null;
    Answer answer = null;
    HashMap<Question, List<Answer>> toReturn = new HashMap<>();

    try {
      sqlQuestion = "SELECT * FROM questions WHERE question=question";
      ResultSet rs = createStatement.executeQuery(sqlQuestion);
      int questionID = rs.getInt("id");
      String questionText = rs.getString("question");
      questionObj = new Question(questionID, questionText);
      rs.close();
    } catch (SQLException ex) {
      System.out.println("Could not add answer! - Question");
      System.out.println("" + ex);
    }

    try {
      sqlAnswer = "SELECT * FROM answers WHERE questionID=question.id";
      ResultSet rs = createStatement.executeQuery(sqlAnswer);
      while (rs.next()) {
        int answerID = rs.getInt("id");
        String answerText = rs.getString("answer");
        int questionID = rs.getInt("questionID");
        answer = new Answer(answerID, answerText, questionID);
        answers.add(answer);
      }
      rs.close();
    } catch (SQLException ex) {
      System.out.println("Could not add answer! - Answer");
      System.out.println("" + ex);
    }
    toReturn.put(questionObj, answers);

    return toReturn;
  }

  //Querys for testing reason
  public void getAllQuestions() {
    try {
      String sql = "SELECT * FROM questions";
      ResultSet rs = createStatement.executeQuery(sql);
      int questionID = rs.getInt("id");
      String q = rs.getString("question");
      System.out.println("Question --> ID: " + questionID + ", QUESTION: " + q);
      rs.close();
    } catch (SQLException ex) {
      System.out.println("Could not add question! - getAQuestionID");
      System.out.println("" + ex);
    }
  }

  public void getAllAnswers() {
    try {
      String sql = "SELECT * FROM answers";
      ResultSet rs = createStatement.executeQuery(sql);
      int ansID = rs.getInt("id");
      String a = rs.getString("question");
      int qID = rs.getInt("questionID");
      System.out.println("Question --> ID: " + ansID + ", QUESTION: " + a + ", QUESTIONID: " + qID);
      rs.close();
    } catch (SQLException ex) {
      System.out.println("Could not add question! - getAQuestionID");
      System.out.println("" + ex);
    }
  }

}
