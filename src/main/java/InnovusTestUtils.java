import dao.Answer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class InnovusTestUtils {

  public String readConsole() {
    Scanner in = new Scanner(System.in);
    return in.nextLine();
  }

  public HashMap<String, List<String>> inputStringParser(String input) {

    String question = "";
    String answer = "";
    List<String> answers = new ArrayList<>();
    HashMap<String, List<String>> toReturn = new HashMap<>();
    int counter = 0;

    int questionmarkPos = input.indexOf('?');
    question = input.substring(0, questionmarkPos);

    String inputStringAnswers = input.substring(questionmarkPos + 1).trim();

    String[] splittedAnswers = inputStringAnswers.split("\".*\"");
    for (int i = 0; i < splittedAnswers.length; i++) {
        answers.add(splittedAnswers[i]);
    }

    toReturn.put(question, answers);
    return toReturn;
  }
}
