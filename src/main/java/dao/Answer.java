package dao;

public class Answer {

    private final int id;
    private String answer;
    private final int questionID;

    private static final String DEFAULT_ANSWER = "The answer to life, universe and everything is 42";

    public Answer(int id, String answer, int questionID) {
        this.id = id;
        this.answer = answer;
        this.questionID = questionID;
    }

    public int getId() {
        return id;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public int getQuestionID() {
        return questionID;
    }

    public static String getDefaultAnswer() {
        return DEFAULT_ANSWER;
    }
}
