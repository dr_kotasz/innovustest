import com.sun.codemodel.internal.JForEach;
import dao.Answer;
import dao.Question;
import db.DB;

import java.util.*;

public class App {

  private static final String INTRO_STRING =
      "Dear User, \n"
          + "I'm a console based question/multiple-answers app.\n"
          + "You can add a question and all of it\'s answers or you can get the answers for a specific question!\n"
          + "The question has to be exactly the same as entered!\n";
  private static final String STRING_TO_CHOOSE_MODE =
      "Do you want to add a question and answers (press \'a\')\n"
          + "or\n"
          + "ask a question (press \'q\')\n"
          + "or\n"
          + "quit (press any other key)?";
  private static final String ADD_A_QUESTION = "Please, add a question!";
  private static final String QUESTION_MANUAL =
      "Adding a question looks like:\n" + "o <question>? \"<answer1>\" \"<answer2>\" \"<answerX>\"";
//  private static final String ADD_AN_ANSWER = "Please, add an answer!";
  private static final String ASK_FOR_ONE_MORE_QUESTION =
      "Do you want to add one more answer?\n" + "YES = y, NO = n";

  private static final String ASK_A_QUESTION = "Please, ask a question!";
  private static final String ANSWERS_ARE = "§ Answers will be";
  private static final String ASK_FOR_ONE_MORE_QUESTIONS =
      "Do you want to ask one more question?\n" + "YES = y, NO = n";

  private static final String GOODBYE = "Goodbye!";

  private static InnovusTestUtils itu = new InnovusTestUtils();
  private static DB db;

  public static void main(String[] args) {

    App app = new App();
    db = new DB();
    System.out.println(INTRO_STRING);
    app.mainMenu();


    //For testing:
//    db.getAllQuestions();
//    db.getAllAnswers();

  }

  private void mainMenu() {

    System.out.println(STRING_TO_CHOOSE_MODE);
    String mode = itu.readConsole();

    if (mode.equals("a")) {
      modeAddQuestionAndAnswers();
    } else if (mode.equals("q")) {
      modeAskAQuestion();
    } else {
      System.out.println(GOODBYE);
    }

  }

  private void modeAddQuestionAndAnswers() {

    String more = "";
    while(!more.equals("n")) {
      System.out.println(ADD_A_QUESTION);
      System.out.println(QUESTION_MANUAL);
      String inputString = itu.readConsole();
      HashMap<String, List<String>> parsedInputs = itu.inputStringParser(inputString);
      for (Map.Entry entry : parsedInputs.entrySet()) {
        String inputQuestion = (String) entry.getKey();
        db.addAQuestion(inputQuestion);
        List<String> actualAnswers = (List<String>) entry.getValue();
        for (String actualAnswer : actualAnswers) {
          db.addAnAnswer(inputQuestion, actualAnswer);
        }
      }
      System.out.println(ASK_FOR_ONE_MORE_QUESTION);
      more = itu.readConsole();
    }

    mainMenu();
  }

  private void modeAskAQuestion() {

    String inputQuestion = "";
    String inputQuestionMore = "";
    Question q = null;

    while (!inputQuestionMore.equals("n")) {
      System.out.println(ASK_A_QUESTION);
      inputQuestion = itu.readConsole();
      HashMap<Question, List<Answer>> answerList = db.getQuestionAndAllAnswers(inputQuestion);

      System.out.println(ANSWERS_ARE);
      List<Answer> answers = answerList.entrySet().iterator().next().getValue();
      for (Answer answer : answers) {
        System.out.println("o " + answer.getAnswer());
      }

      System.out.println(ASK_FOR_ONE_MORE_QUESTIONS);
      inputQuestionMore = itu.readConsole();
    }

    mainMenu();
  }
}
